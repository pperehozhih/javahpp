//
//  javahppclassinfo.cpp
//  javahpp
//
//  Created by Paul Perekhozhikh on 07.08.15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#include "javahppclassinfo.h"

namespace javahpp {
   namespace types {
      RootClassInfo GetRootClassInfo(const parser::Parser& parser){
         RootClassInfo result;
         const types::class_header& header = parser.GetHeader();
         types::CONSTANT_Class_info root_info = *(types::CONSTANT_Class_info*)header.constant_pool[header.this_class];
         result.fulle_name = parser.GetCppString(parser::Parser::EndianScalar(root_info.name_index));
         result.name = result.fulle_name.substr(result.fulle_name.rfind('/') + 1);
         result.name_space = result.fulle_name.substr(0, result.fulle_name.rfind('/'));
         for (types::method_info_ex method_info : header.methods){
            MethodInfo method;
            uint16_t flags = method_info.access_flags;
            method.name = parser.GetCppString(method_info.name_index);
            method.description = parser.GetCppString(method_info.descriptor_index);
            method.is_public = flags & access_flags::PUBLIC;
            method.is_private = flags & access_flags::PRIVATE;
            method.is_protected = flags & access_flags::PROTECTED;
            method.is_static = flags & access_flags::STATIC;
            method.is_native = flags & access_flags::NATIVE;
            result.methods.push_back(method);
         }
         return result;
      }
   }
}
