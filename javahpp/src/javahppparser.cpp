//
//  javahppparser.cpp
//  javahpp
//
//  Created by Paul Perekhozhikh on 07.08.15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#include "javahppparser.h"

namespace javahpp {
   namespace parser {
      void Parser::Parse() {
         SeekSetBegin(0);
         m_header.magic = ReadScalar<uint32_t>();
         assert(m_header.magic == JAVA_MAGIC);
         m_header.minor_version = ReadScalar<uint16_t>();
         m_header.major_version = ReadScalar<uint16_t>();
         m_header.constant_pool_count = ReadScalar<uint16_t>();
         ParseConstant();
         m_header.access_flags = ReadScalar<uint16_t>();
         m_header.this_class = ReadScalar<uint16_t>();
         m_header.super_class = ReadScalar<uint16_t>();
         m_header.interfaces_count = ReadScalar<uint16_t>();
         ParseInterface();
         m_header.fields_count = ReadScalar<uint16_t>();
         ParseField();
         m_header.methods_count = ReadScalar<uint16_t>();
         ParseMethod();
      }
      
      const char* Parser::GetString(uint16_t id){
         if (m_header.constant_pool.size() < id){
            return nullptr;
         }
         if (m_header.constant_pool[id]->tag == types::CONSTANT_String){
            return GetString(((types::CONSTANT_String_info*)m_header.constant_pool[id])->string_index);
         }
         if (m_header.constant_pool[id]->tag != types::CONSTANT_Utf8){
            return nullptr;
         }
         return ((const char*)m_header.constant_pool[id]) + sizeof(uint16_t) + sizeof(uint8_t);
      }
      
      std::string Parser::GetCppString(uint16_t id) const{
         if (m_header.constant_pool.size() < id){
            return "";
         }
         if (m_header.constant_pool[id]->tag == types::CONSTANT_String){
            return GetCppString(((types::CONSTANT_String_info*)m_header.constant_pool[id])->string_index);
         }
         if (m_header.constant_pool[id]->tag != types::CONSTANT_Utf8){
            return "";
         }
         const types::CONSTANT_Utf8_info* info = (const types::CONSTANT_Utf8_info*)m_header.constant_pool[id];
         const char* str = ((const char*)m_header.constant_pool[id]) + sizeof(uint16_t) + sizeof(uint8_t);
         return std::string(str, EndianScalar(info->length));
      }
      
      size_t Parser::CalcConstantSize(){
         types::cp_info* info = (types::cp_info*)(m_iterator);
         switch (info->tag) {
            case types::CONSTANT_Integer:{
               return sizeof(types::CONSTANT_Integer_info);
            }
            case types::CONSTANT_Float:{
               return sizeof(types::CONSTANT_Float_info);
            }
            case types::CONSTANT_Long:{
               return sizeof(types::CONSTANT_Long_info);
            }
            case types::CONSTANT_Double:{
               return sizeof(types::CONSTANT_Double_info);
            }
            case types::CONSTANT_Utf8:{
               size_t res = sizeof(types::CONSTANT_Utf8_info) - sizeof(uint8_t*);
               res += EndianScalar(((types::CONSTANT_Utf8_info*)info)->length);
               return res;
            }
            case types::CONSTANT_String:{
               return sizeof(types::CONSTANT_String_info);
            }
            case types::CONSTANT_Class:{
               return sizeof(types::CONSTANT_Class_info);
            }
            case types::CONSTANT_Fieldref:{
               return sizeof(types::CONSTANT_Fieldref_info);
            }
            case types::CONSTANT_Methodref:{
               return sizeof(types::CONSTANT_Methodref_info);
            }
            case types::CONSTANT_InterfaceMethodref:{
               return sizeof(types::CONSTANT_InterfaceMethodref_info);
            }
            case types::CONSTANT_NameAndType:{
               return sizeof(types::CONSTANT_NameAndType_info);
            }
            default:
               assert(false);
         }
      }
      
      void Parser::ParseConstant() {
         m_header.constant_pool.clear();
         m_header.constant_pool.reserve(m_header.constant_pool_count - 1);
         m_header.constant_pool.push_back(nullptr);
         for(int i = 1; i < m_header.constant_pool_count; i++){
            m_header.constant_pool.push_back((types::cp_info*)(m_iterator));
            SeekSetCurrent(CalcConstantSize());
            if(m_header.constant_pool[i]->tag == types::CONSTANT_Long || m_header.constant_pool[i]->tag ==
               types::CONSTANT_Double)
            {
               m_header.constant_pool.push_back(nullptr);
               i++;
            }
         }
      }
      
      void Parser::ParseInterface(){
         m_header.interfaces.clear();
         m_header.interfaces.reserve(m_header.interfaces_count);
         for (int i = 0; i < m_header.interfaces_count; i++) {
            m_header.interfaces.push_back(ReadScalar<uint16_t>());
         }
      }
      
      void Parser::ParseField(){
         m_header.fields.resize(m_header.fields_count);
         for (int i = 0; i < m_header.fields_count; i++) {
            m_header.fields[i].base = (const types::field_info*)m_iterator;
            m_header.fields[i].access_flags = ReadScalar<uint16_t>(); //access_flags
            m_header.fields[i].name_index = ReadScalar<uint16_t>(); //
            m_header.fields[i].descriptor_index = ReadScalar<uint16_t>(); //
            m_header.fields[i].attributes_count = ReadScalar<uint16_t>();
            m_header.fields[i].name = GetString(m_header.fields[i].name_index);
            m_header.fields[i].description = GetString(m_header.fields[i].descriptor_index);
            if(m_header.fields[i].attributes_count > 0){
               //skip attributes - we do not need in simple cases
               for(int a=0;a<m_header.fields[i].attributes_count;a++)
               {
                  ReadScalar<uint16_t>();
                  //printf("Attribute name index = %d\n", name_index);
                  uint32_t len = ReadScalar<uint32_t>();
                  SeekSetCurrent(len);
               }
            }
         }
      }
      
      void Parser::ParseCode(int index){
         types::code_attribute_ex code;
         code.base = (types::code_attribute*)m_iterator;
         
         code.attribute_name_index = ReadScalar<uint16_t>();
         code.name = GetString(code.attribute_name_index);
         code.attribute_length = ReadScalar<uint32_t>();
         code.max_stack = ReadScalar<uint16_t>();
         code.max_locals = ReadScalar<uint16_t>();
         code.code_length = ReadScalar<uint32_t>();
         code.code = (const uint8_t*)m_iterator;
         SeekSetCurrent(code.code_length);
         code.exception_table_length = ReadScalar<uint16_t>();
         for (int i = 0; i < code.exception_table_length; i++){
            SeekSetCurrent(sizeof(types::Exception_table));// Skip exception table
         }
         code.attributes_count = ReadScalar<uint16_t>();
         for(int a = 0;a < code.attributes_count; a++) // Skip attribute
         {
            ReadScalar<uint16_t>();
            uint32_t len = ReadScalar<uint32_t>();
            SeekSetCurrent(len);
         }
         m_header.methods[index].code_attr.push_back(code);
      }
      
      void Parser::ParseMethod() {
         m_header.methods.resize(m_header.methods_count);
         for (int i = 0; i < m_header.methods_count; i++) {
            m_header.methods[i].base = (const types::method_info*)m_iterator;
            m_header.methods[i].access_flags = ReadScalar<uint16_t>();
            m_header.methods[i].name_index = ReadScalar<uint16_t>();
            m_header.methods[i].descriptor_index = ReadScalar<uint16_t>();
            m_header.methods[i].attributes_count = ReadScalar<uint16_t>();
            m_header.methods[i].name = GetString(m_header.methods[i].name_index);
            m_header.methods[i].description = GetString(m_header.methods[i].descriptor_index);
            for (int a = 0; a < m_header.methods[i].attributes_count; a++){
               ReadScalar<uint16_t>();
               SeekSetCurrent(ReadScalar<uint32_t>());
//               ParseCode(i);
            }
         }
      }
   }
}
