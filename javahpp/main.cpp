//
//  main.cpp
//  javahpp
//
//  Created by Paul Perekhozhikh on 07.08.15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "javahppgen.h"

struct Config {
   std::string inputname;
   std::string outputdir;
   inline bool IsComplite(){
      return inputname.length() != 0;
   }
   
   bool parseparm(const char* param, const char* nextparam){
      if (param == nullptr || param[0] != '-' || param[0] == 0)
         return false;
      switch(param[1]){
         case 'c':{
            if (nextparam){
               inputname = nextparam;
               return true;
            }
         }
         case 'o':{
            if (nextparam){
               outputdir = nextparam;
               return true;
            }
         }
      }
      return false;
   }
};

struct ParseFile{
   static int Parse(Config& cfg){
      FILE* f = fopen(cfg.inputname.c_str(), "r");
      if (f == nullptr){
         std::cout << "Cannot open file - " << cfg.inputname << std::endl;
         return -2;
      }
      fseek(f, 0, SEEK_END);
      size_t size = ftell(f);
      
      char* where = new char[size];
      
      rewind(f);
      fread(where, sizeof(char), size, f);
      
      javahpp::parser::Parser parser(where);
      parser.Parse();
      javahpp::types::RootClassInfo info = javahpp::types::GetRootClassInfo(parser);
      javahpp::gen::CodeGeneratorBase codegen;
      
      if (cfg.outputdir.length() && cfg.outputdir.back() != '/'){
         cfg.outputdir.push_back('/');
      }
      
      std::string file_name = cfg.outputdir + javahpp::gen::RemoveBadSymbolsForName(info.fulle_name);
      
      std::stringstream stream_header = codegen.GenHeader(info);
      std::stringstream stream_cpp = codegen.GenSource(info);
      
      {
         std::ofstream header_file(file_name + ".h");
         header_file << stream_header.str() << std::endl;
      }
      
      {
         std::ofstream cpp_file(file_name + ".cpp");
         cpp_file << "#include \"" << javahpp::gen::RemoveBadSymbolsForName(info.fulle_name) << ".h\" " << std::endl;
         cpp_file << stream_cpp.str() << std::endl;
      }
      
      delete[] where;
      return 0;
   }
};

void PrintUsage(){
   std::cout << "Please use key" << std::endl;
   std::cout << "\tc - <class file>" << std::endl;
   std::cout << "\to - <output dir>" << std::endl;
}

int main(int argc, const char * argv[]) {
   
   Config cfg;
   int index = 1;
   while(argv[index]){
      if (cfg.parseparm(argv[index], argv[index + 1]))
         index++;
      index++;
   }
   if (cfg.IsComplite()){
      return ParseFile::Parse(cfg);
   } else {
      PrintUsage();
   }
   return -1;
}
