//
// Created by Paul Perekhozhikh on 06.08.15.
//

#ifndef ANDROID_JNI_HELPER_H
#define ANDROID_JNI_HELPER_H

#include <jni.h>
#include <string>
#include <vector>

namespace javahpp {
    namespace jni {

        JavaVM* GetVM();

        JNIEnv* GetEnv();

        jclass GetClass(const char* name);

        namespace inner{
            template<typename T>
            inline const std::string to_name();

            template<>
            inline const std::string to_name<std::string>(){
                return "Ljava/lang/String;";
            }

            template<>
            inline const std::string to_name<const char*>(){
                return "Ljava/lang/String;";
            }

            template<>
            inline const std::string to_name<bool>(){
                return "Z";
            }

            template<>
            inline const std::string to_name<unsigned char>(){
                return "B";
            }

            template<>
            inline const std::string to_name<char>(){
                return "C";
            }

            template<>
            inline const std::string to_name<short>(){
                return "S";
            }

            template<>
            inline const std::string to_name<long>(){
                return "J";
            }

            template<>
            inline const std::string to_name<int>() {
                return "I";
            }

            template<>
            inline const std::string to_name<double>() {
                return "D";
            }

            template<>
            inline const std::string to_name<float>() {
                return "F";
            }

            template<>
            inline const std::string to_name<void>() {
                return "V";
            }

            template <typename MethodType, typename... Args>
            inline std::string get_signature(Args... args)
            {
                const size_t arg_num = sizeof...(Args);
                std::string signatures[arg_num] = { to_name<Args>()... };
                std::string signature_string = "(";
                for (size_t i = 0; i < arg_num; ++i)
                    signature_string += signatures[i];
                signature_string += ")";
                signature_string += to_name<MethodType>();
                return signature_string;
            }

            struct object_holder {
                jobject jObject;
                JNIEnv* m_env;

                object_holder()
                        : m_env(nullptr)
                {}

                object_holder(const object_holder& v){
                    jObject = v.jObject;
                    m_env = v.m_env;
                }

                object_holder(JNIEnv* env, jobject obj)
                        : jObject(obj)
                        , m_env(env)
                {
                }

                ~object_holder()
                {
                    if (jObject && m_env != nullptr) {
                        m_env->DeleteLocalRef(jObject);
                    }
                }

                jobject get() { return jObject; }
            };

            struct value_holder {
                std::shared_ptr<object_holder> object;
                jvalue        value;
                explicit value_holder(JNIEnv* env, bool val) {
                    value.z = val;
                }

                explicit value_holder(JNIEnv* env, float val) {
                    value.f = val;
                }

                explicit value_holder(JNIEnv* env, double val) {
                    value.d = val;
                }

                explicit value_holder(JNIEnv* env, unsigned char val) {
                    value.b = val;
                }

                explicit value_holder(JNIEnv* env, char val) {
                    value.c = val;
                }

                explicit value_holder(JNIEnv* env, short val) {
                    value.s = val;
                }

                explicit value_holder(JNIEnv* env, int val) {
                    value.i = val;
                }

                explicit value_holder(JNIEnv* env, long val) {
                    value.j = val;
                }

                explicit value_holder(JNIEnv* env, const char* str):object(new object_holder(env, env->NewStringUTF(str))) {
                    value.l = object->jObject;
                }

                explicit value_holder(JNIEnv* env, const std::string& str):object(new object_holder(env, env->NewStringUTF(str.c_str()))) {
                    value.l = object->jObject;
                }

                explicit value_holder(JNIEnv* env, std::string& str):object(new object_holder(env, env->NewStringUTF(str.c_str()))) {
                    value.l = object->jObject;
                }

                explicit value_holder(void*){}

                operator jvalue() {
                    return value;
                }
            };

            template <typename... Args>
            struct values_data{
                std::vector<value_holder> holders;
                std::vector<jvalue> values;
                size_t size;
                values_data(JNIEnv* env, Args... args){
                    size = sizeof...(args);
                    if (size != 0) {
                        values.resize(size);
                        holders = {value_holder(env, args)...};
                        size_t pos = size;
                        do {
                            pos--;
                            values[pos] = holders[pos];
                        } while (pos);
                    }
                }
                inline jvalue* get_values(){
                    return &values[0];
                }
            };

            template <typename MethodType>
            struct impl
            {
                template <typename... Args>
                static MethodType call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args){
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return MethodType(env->CallStaticObjectMethodA(clazz, method, datas.get_values()));
                    }else{
                        return MethodType(env->CallStaticObjectMethod(clazz, method));
                    }
                }

                template <typename... Args>
                static MethodType call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args){
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return MethodType(env->CallObjectMethodA(obj, method, datas.get_values()));
                    }else{
                        return MethodType(env->CallObjectMethod(obj, method));
                    }
                }
            };

            template <>
            struct  impl<bool>
            {
                template <typename... Args>
                static bool call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args) {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticBooleanMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticBooleanMethod(clazz, method);
                    }
                }

                template <typename... Args>
                static bool call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args) {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallBooleanMethodA(obj, method, datas.get_values());
                    }else{
                        return env->CallBooleanMethod(obj, method);
                    }
                }
            };

            template <>
            struct  impl<unsigned char>
            {
                template <typename... Args>
                static unsigned char call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args) {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticByteMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticByteMethod(clazz, method);
                    }
                }

                template <typename... Args>
                static unsigned char call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args) {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallByteMethodA(obj, method, datas.get_values());
                    }else{
                        return env->CallByteMethod(obj, method);
                    }
                }
            };

            template <>
            struct  impl<char>
            {
                template <typename... Args>
                static unsigned char call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args) {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticCharMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticCharMethod(clazz, method);
                    }
                }

                template <typename... Args>
                static unsigned char call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args) {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallCharMethodA(obj, method, datas.get_values());
                    }else{
                        return env->CallCharMethod(obj, method);
                    }
                }
            };

            template <>
            struct  impl<short>
            {
                template <typename... Args>
                static short call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args) {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticShortMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticShortMethod(clazz, method);
                    }
                }

                template <typename... Args>
                static short call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args) {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallShortMethodA(obj, method, datas.get_values());
                    }else{
                        return env->CallShortMethod(obj, method);
                    }
                }
            };

            template <>
            struct  impl<int>
            {
                template <typename... Args>
                static int call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticIntMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticIntMethod(clazz, method);
                    }
                }

                template <typename... Args>
                static int call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallIntMethodA(obj, method, datas.get_values());
                    }else{
                        return env->CallIntMethod(obj, method);
                    }
                }
            };

            template <>
            struct  impl<long>
            {
                template <typename... Args>
                static long call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticLongMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticLongMethod(clazz, method);
                    }
                }

                template <typename... Args>
                static long call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallLongMethodA(obj, method, datas.get_values());
                    }else{
                        return env->CallLongMethod(obj, method);
                    }
                }
            };

            template <>
            struct  impl<float>
            {
                template <typename... Args>
                static float call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticFloatMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticFloatMethod(clazz, method);
                    }
                }
            };

            template <>
            struct  impl<double>
            {
                template <typename... Args>
                static double call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticDoubleMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticDoubleMethod(clazz, method);
                    }
                }

                template <typename... Args>
                static double call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallDoubleMethodA(obj, method, datas.get_values());
                    }else{
                        return env->CallDoubleMethod(obj, method);
                    }
                }
            };

            template <>
            struct  impl<std::string>
            {
                template <typename... Args>
                static std::string call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args)
                {
                    jstring res = nullptr;
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        res = (jstring)env->CallStaticObjectMethodA(clazz, method, datas.get_values());
                    }else{
                        res = (jstring)env->CallStaticObjectMethod(clazz, method);
                    }
                    jboolean is_copy = JNI_FALSE;
                    const char* char_res= env->GetStringUTFChars(res, &is_copy);
                    std::string result = char_res;
                    if (is_copy == JNI_TRUE) {
                        env->ReleaseStringUTFChars(res, char_res);
                    }
                    return result;
                }

                template <typename... Args>
                static std::string call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args)
                {
                    jstring res = nullptr;
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        res = (jstring)env->CallObjectMethodA(obj, method, datas.get_values());
                    }else{
                        res = (jstring)env->CallObjectMethod(obj, method);
                    }
                    jboolean is_copy = JNI_FALSE;
                    const char* char_res= env->GetStringUTFChars(res, &is_copy);
                    std::string result = char_res;
                    if (is_copy == JNI_TRUE) {
                        env->ReleaseStringUTFChars(res, char_res);
                    }
                    return result;
                }
            };

            template <>
            struct impl<void>
            {
                template <typename... Args>
                static void call_static_method(JNIEnv* env, jclass clazz, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallStaticVoidMethodA(clazz, method, datas.get_values());
                    }else{
                        return env->CallStaticVoidMethod(clazz, method);
                    }
                }

                template <typename... Args>
                static void call_method(JNIEnv* env, jobject obj, jmethodID method, Args... args)
                {
                    values_data<Args...> datas(env, args...);
                    if (datas.size){
                        return env->CallVoidMethodA(obj, method, datas.get_values());
                    }else{
                        return env->CallVoidMethod(obj, method);
                    }
                }
            };
        };

        template <typename MethodType, typename... Args>
        MethodType execute_static_method(const std::string& clazz, const std::string &name, Args... args){

            std::string signature = inner::get_signature<MethodType>(args...);
            JNIEnv* env = GetEnv();

            jclass cls = GetClass(clazz.c_str());

            jmethodID method = env->GetStaticMethodID(cls, name.c_str(), signature.c_str());

            return inner::impl<MethodType>::call_static_method(env, cls, method, args...);
        }

        template <typename MethodType, typename... Args>
        MethodType execute_method(const std::string& clazz, const std::string &name, jobject obj, Args... args){

            std::string signature = inner::get_signature<MethodType>(args...);
            JNIEnv* env = GetEnv();

            jclass cls = GetClass(clazz.c_str());

            jmethodID method = env->GetMethodID(cls, name.c_str(), signature.c_str());

            return inner::impl<MethodType>::call_method(env, obj, method, args...);
        }

        template<char const* clazz>
        struct jni_class_wrapper{
            jobject m_current_object;
            jni_class_wrapper(jobject obj){
                m_current_object = GetEnv()->NewGlobalRef(obj);
            }
            jni_class_wrapper(const jni_class_wrapper& v){
                m_current_object = GetEnv()->NewGlobalRef(v.m_current_object);
            }
            virtual ~jni_class_wrapper(){
                JNIEnv* env = GetEnv();
                env->DeleteGlobalRef(m_current_object);
            }
            jni_class_wrapper():m_current_object(nullptr){}

            template<char* method, typename MethodType, typename... Args>
            struct static_method{
                MethodType operator()(Args... args){
                    return execute_static_method<MethodType>(clazz, method, args...);
                }
            };

            template<char* method, typename MethodType, typename... Args>
            struct object_method{
                jni_class_wrapper* wrapper = nullptr;
                object_method(const jni_class_wrapper* wrapper):wrapper(wrapper){}
                object_method(const object_method& o):wrapper(o.wrapper){}
                MethodType operator()(Args... args){
                    return execute_method<MethodType>(clazz, method, wrapper->m_current_object, args...);
                }
            };
        };
    }
}

#define JNI_HELPER_FUNCTION(name, result, ...) static_method< #name, result, __VA_ARGS__> name;

#endif //ANDROID_JNI_HELPER_H
