//
//  javahppparser.h
//  javahpp
//
//  Created by Paul Perekhozhikh on 07.08.15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#ifndef javahpp_javahppparser_h
#define javahpp_javahppparser_h

#if !defined(JAVAHPP_LITTLEENDIAN)
   #if defined(__GNUC__) || defined(__clang__)
      #ifdef __BIG_ENDIAN__
         #define JAVAHPP_LITTLEENDIAN 0
      #else
         #define JAVAHPP_LITTLEENDIAN 1
      #endif // __BIG_ENDIAN__
   #elif defined(_MSC_VER)
      #if defined(_M_PPC)
         #define JAVAHPP_LITTLEENDIAN 0
      #else
         #define JAVAHPP_LITTLEENDIAN 1
      #endif
   #else
      #error Unable to determine endianness, define JAVAHPP_LITTLEENDIAN.
   #endif
#endif // !defined(JAVAHPP_LITTLEENDIAN)

#include <string>
#include <stack>
#include <cassert>
#include "javahppstructs.h"

namespace javahpp {
   namespace parser {
      const uint32_t JAVA_MAGIC = 0xCAFEBABE;
      class Parser {
      public:
         template<typename T> static T EndianScalar(T t){
#ifndef JAVAHPP_LITTLEENDIAN
            return t;
#else
   #if defined(_MSC_VER)
      #pragma push_macro("__builtin_bswap16")
      #pragma push_macro("__builtin_bswap32")
      #pragma push_macro("__builtin_bswap64")
      #define __builtin_bswap16 _byteswap_ushort
      #define __builtin_bswap32 _byteswap_ulong
      #define __builtin_bswap64 _byteswap_uint64
   #endif
            if (sizeof(T) == 1) {
               return t;
            } else if (sizeof(T) == 2) {
               auto r = __builtin_bswap16(*reinterpret_cast<uint16_t *>(&t));
               return *reinterpret_cast<T *>(&r);
            } else if (sizeof(T) == 4) {
               auto r = __builtin_bswap32(*reinterpret_cast<uint32_t *>(&t));
               return *reinterpret_cast<T *>(&r);
            } else if (sizeof(T) == 8) {
               auto r = __builtin_bswap64(*reinterpret_cast<uint64_t *>(&t));
               return *reinterpret_cast<T *>(&r);
            } else {
               assert(0);
            }
   #if defined(_MSC_VER)
      #pragma pop_macro("__builtin_bswap16")
      #pragma pop_macro("__builtin_bswap32")
      #pragma pop_macro("__builtin_bswap64")
   #endif
#endif
         }
      private:
         const char* m_buffer = nullptr;
         const char* m_iterator = nullptr;
         javahpp::types::class_header m_header;
         template<typename T>
         T ReadScalar(){
            const char* result = m_iterator;
            m_iterator += sizeof(T);
            return EndianScalar(*(reinterpret_cast<const T*>((void*)result)));
         }
         template<typename T>
         void ReadStruct(T* t){
            memcpy(t, m_iterator, sizeof(T));
            m_iterator += sizeof(T);
         }
         inline void SeekSetCurrent(size_t offset){
            m_iterator += offset;
         }
         inline void SeekSetBegin(size_t offset){
            m_iterator = m_buffer + offset;
         }
         const char* GetString(uint16_t id);
         size_t CalcConstantSize();
         void ParseCode(int index);
         void ParseConstant();
         void ParseInterface();
         void ParseField();
         void ParseMethod();
      public:
         Parser(const char* buffer):m_buffer(buffer), m_iterator(buffer){}
         void Parse();
         inline const javahpp::types::class_header& GetHeader() const{
            return m_header;
         }
         std::string GetCppString(uint16_t id) const;
      };
   }
}

#endif
