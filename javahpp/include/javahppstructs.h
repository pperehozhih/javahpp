//
//  javahppstructs.h
//  javahpp
//
//  Created by Paul Perekhozhikh on 07.08.15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#ifndef javahpp_javahppstructs_h
#define javahpp_javahppstructs_h

#include <stdint.h>
#include <vector>

namespace javahpp {
   namespace types {
      
      const uint32_t CONSTANT_Integer = 3;
      const uint32_t CONSTANT_Float = 4;
      const uint32_t CONSTANT_Long = 5;
      const uint32_t CONSTANT_Double = 6;
      const uint32_t CONSTANT_Utf8 = 1;
      const uint32_t CONSTANT_String = 8;
      
      const uint32_t CONSTANT_Class = 7;
      const uint32_t CONSTANT_Fieldref = 9;
      const uint32_t CONSTANT_Methodref = 10;
      const uint32_t CONSTANT_InterfaceMethodref = 11;
      const uint32_t CONSTANT_NameAndType = 12;
      
      #pragma pack(push, 1)
      
      struct cp_info {
         uint8_t tag;
         uint8_t* info;
      };
      
      struct attribute_info {
         uint16_t attribute_name_index;
         uint32_t attribute_length;
         uint8_t* info;//[attribute_length];
      };
      
      struct Exception_table
      {
         uint16_t start_pc;
         uint16_t end_pc;
         uint16_t handler_pc;
         uint16_t catch_type;
      };
      
      #pragma pack(pop)
      
      struct field_info {
         uint16_t access_flags;
         uint16_t name_index;
         uint16_t descriptor_index;
         uint16_t attributes_count;
         attribute_info* attributes; //[attributes_count];
      };
      
      struct field_info_ex : public field_info {
         const field_info* base;
         const char* name = nullptr;
         const char* description = nullptr;
      };
      
      struct code_attribute {
         uint16_t attribute_name_index;
         uint32_t attribute_length;
         uint16_t max_stack;
         uint16_t max_locals;
         uint32_t code_length;
         const uint8_t* code;//[code_length];
         uint16_t exception_table_length;
         Exception_table* exception_table;//[exception_table_length];
         uint16_t attributes_count;
         attribute_info* attributes;//[attributes_count];
      };
      
      struct code_attribute_ex : public code_attribute{
         const code_attribute* base;
         const char* name = nullptr;
      };
      
      struct method_info {
         uint16_t access_flags;
         uint16_t name_index;
         uint16_t descriptor_index;
         uint16_t attributes_count;
         attribute_info* attributes; //[attributes_count];
      };
      
      struct method_info_ex : public method_info {
         const method_info* base;
         std::vector<code_attribute> code_attr;
         const char* name = nullptr;
         const char* description = nullptr;
      };
      
      struct  class_header {
         uint32_t magic;
         uint16_t minor_version;
         uint16_t major_version;
         uint16_t constant_pool_count;
         std::vector<cp_info*>constant_pool; //[constant_pool_count-1];
         uint16_t access_flags;
         uint16_t this_class;
         uint16_t super_class;
         uint16_t interfaces_count;
         std::vector<uint16_t> interfaces; //[interfaces_count];
         uint16_t fields_count;
         std::vector<field_info_ex> fields; //[fields_count];
         uint16_t methods_count;
         std::vector<method_info_ex> methods; //[methods_count];
         uint16_t attributes_count;
         attribute_info** attributes; //[attributes_count];
      };
      
      #pragma pack(push, 1)
      
      struct CONSTANT_Integer_info {
         uint8_t tag;
         uint32_t bytes;
      };
      
      struct CONSTANT_Float_info {
         uint8_t tag;
         uint32_t bytes;
      };
      
      struct CONSTANT_Long_info {
         uint8_t  tag;
         uint32_t high_bytes;
         uint32_t low_bytes;
      };
      
      struct CONSTANT_Double_info {
         uint8_t tag;
         uint32_t high_bytes;
         uint32_t low_bytes;
      };
      
      struct CONSTANT_Utf8_info {
         uint8_t tag;
         uint16_t length;
         char* bytes;//[length];
      };
      
      struct CONSTANT_String_info {
         uint8_t tag;
         uint16_t string_index;
      };
      
      struct CONSTANT_Class_info {
         uint8_t tag;
         uint16_t name_index;
      };
      
      struct CONSTANT_Fieldref_info {
         uint8_t  tag;
         uint16_t class_index;
         uint16_t name_and_type_index;
      };
      
      struct CONSTANT_Methodref_info {
         uint8_t tag;
         uint16_t class_index;
         uint16_t name_and_type_index;
      };
      
      struct CONSTANT_InterfaceMethodref_info {
         uint8_t tag;
         uint16_t class_index;
         uint16_t name_and_type_index;
      };
      
      struct CONSTANT_NameAndType_info {
         uint8_t tag;
         uint16_t name_index;
         uint16_t descriptor_index;
      };
      
      #pragma pack(pop)
   }
}

#endif
