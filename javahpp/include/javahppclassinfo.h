//
//  javahppclassinfo.h
//  javahpp
//
//  Created by Paul Perekhozhikh on 07.08.15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#ifndef javahpp_javahppclassinfo_h
#define javahpp_javahppclassinfo_h

#include <string>
#include <vector>
#include <stdint.h>
#include "javahppparser.h"

namespace javahpp {
   namespace types {
      namespace access_flags{
         enum AccessFlag{
            PUBLIC = 1,
            PRIVATE = 2,
            PROTECTED = 4,
            STATIC = 8,
            NATIVE = 256
         };
      }
      
      struct FieldInfo{
         std::string name;
         std::string type;
         bool is_public;
         bool is_private;
         bool is_protected;
         bool is_static;
      };
      
      struct MethodInfo {
         std::string name;
         std::string description;
         bool is_public;
         bool is_private;
         bool is_protected;
         bool is_static;
         bool is_native;
      };
      
      struct RootClassInfo {
         std::string fulle_name;
         std::string name;
         std::string name_space;
         std::vector<FieldInfo> fields;
         std::vector<MethodInfo> methods;
      };
      
      RootClassInfo GetRootClassInfo(const parser::Parser&);
   }
}

#endif
