//
//  javahpputils.h
//  javahpp
//
//  Created by Paul Perekhozhikh on 08.08.15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#ifndef javahpp_javahpputils_h
#define javahpp_javahpputils_h

#include <vector>

namespace javahpp {
   namespace utils {
      inline std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
         std::stringstream ss(s);
         std::string item;
         while (std::getline(ss, item, delim)) {
            elems.push_back(item);
         }
         return elems;
      }
      
      
      inline std::vector<std::string> split(const std::string &s, char delim) {
         std::vector<std::string> elems;
         split(s, delim, elems);
         return elems;
      }
   }
}

#endif
