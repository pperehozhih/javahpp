//
//  javahppgen.h
//  javahpp
//
//  Created by Paul Perekhozhikh on 07.08.15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#ifndef javahpp_javahppgen_h
#define javahpp_javahppgen_h

#include <sstream>
#include <map>
#include "javahppparser.h"
#include "javahppclassinfo.h"

namespace javahpp {
   namespace gen {
      
      std::string RemoveBadSymbolsForName(std::string str);
      
      class CodeGeneratorBase;
      
      struct CodeGeneratorPage {
         virtual std::string GetHead(const types::RootClassInfo& class_info, CodeGeneratorBase* gen) = 0;
         virtual std::string GetFooter(const types::RootClassInfo& class_info, CodeGeneratorBase* gen) = 0;
      };
      
      struct CodeGeneratorMethod {
         virtual std::string GetDeclaration(const types::RootClassInfo& class_info, const types::MethodInfo& method) = 0;
         virtual std::string GetMethod(const types::RootClassInfo& class_info, const types::MethodInfo& method) = 0;
         virtual std::string GetStatic(const types::RootClassInfo& class_info, const types::MethodInfo& method) = 0;
      };
      
      struct CodeGeneratorClass {
         virtual std::string GetStartClass(const types::RootClassInfo& class_info, CodeGeneratorBase* gen) = 0;
         virtual std::string GetEndClass(const types::RootClassInfo& class_info, CodeGeneratorBase* gen) = 0;
      };
      
      struct CodeGeneratorPageDefault : public CodeGeneratorPage{
         virtual std::string GetHead(const types::RootClassInfo& class_info, CodeGeneratorBase* gen);
         virtual std::string GetFooter(const types::RootClassInfo& class_info, CodeGeneratorBase* gen);
      };
      
      struct CodeGeneratorClassDefault : public CodeGeneratorClass {
         virtual std::string GetStartClass(const types::RootClassInfo& class_info, CodeGeneratorBase* gen);
         virtual std::string GetEndClass(const types::RootClassInfo& class_info, CodeGeneratorBase* gen);
      };
      
      class CodeGeneratorBase{
      protected:
         CodeGeneratorPage* m_page_gen = nullptr;
         CodeGeneratorClass* m_class_gen = nullptr;
         CodeGeneratorMethod* m_method_gen = nullptr;
         std::shared_ptr<CodeGeneratorPage> m_page_gen_default;
         std::shared_ptr<CodeGeneratorClass> m_class_gen_default;
         std::shared_ptr<CodeGeneratorMethod> m_method_gen_default;
         std::map<std::string, std::string> m_values_java_to_jni_map;
         std::map<std::string, std::string> m_values_java_to_cpp_map;
         std::string m_current_prefix;
      public:
         CodeGeneratorBase();
         std::stringstream GenHeader(const types::RootClassInfo&);
         std::stringstream GenSource(const types::RootClassInfo&);
         inline void SetGenPage(CodeGeneratorPage* page){
            m_page_gen = page;
         }
         inline void SetGenClass(CodeGeneratorClass* cls){
            m_class_gen = cls;
         }
         inline void SetGenMethod(CodeGeneratorMethod* method){
            m_method_gen = method;
         }
         inline const std::string& GetCurrentPrefix(){
            return m_current_prefix;
         }
         inline void PopCurrentPrefix(){
            if (m_current_prefix.length()){
               m_current_prefix.pop_back();
            }
         }
         inline void PushCurrentPrefix(char c){
            m_current_prefix.push_back(c);
         }
         std::string SignatureToVariable(const std::string& signature_string,
                                         const std::map<std::string, std::string>& map,
                                         const std::string& default_value);
         std::string SignatureToVariableJNI(const std::string& signature_string);
         std::string SignatureToVariableCPP(const std::string& signature_string);
         inline CodeGeneratorPage* GetGenPage(){return m_page_gen;}
         inline CodeGeneratorClass* GetGenClass(){return m_class_gen;}
         inline CodeGeneratorMethod* GetGenMethod(){return m_method_gen;}

      };
   }
}

#endif
